// * Nuestra informacion se encuentra aislada
const m = {
  es:{
    dataText: 'mi primer mvc',
    dataHead: 'Buen trabajo!!'
  },
  en:{
    dataText: 'en ingles',
    dataHead: 'en ingles mas ingles!!'
  },
  verDataLocalStorage : function(){
    const idioma = localStorage.getItem('idioma');
    if(idioma){
      return idioma;
    }else{
      return 'en'
    }
  }
}

// * nuestra vista se encarga de mostrar en panalla lo que requerimos
// * tambien se va a encargar de las interacciones desde el cliente
const v = {
  renderAlert: function(data){
    swal(data.dataHead, data.dataText, "success");
  },
  renderBody: function(data){
    const newContentText = document.getElementById('textContent')
    newContentText.innerHTML= ` <h1 class="title">${data.dataHead}</h1><h2 class="subtitle">
    ${data.dataText}</h2> `;
  },
  configButton: function(dataUsuario){
  const repitoFuncion = this.renderAlert
  console.log(repitoFuncion)
    document.getElementById('alertCta').addEventListener("click", function (event) {
      repitoFuncion(dataUsuario)
      console.log('esto fue idea de edu')
    }, false);
  },
 }

// * El controlador se ecncarga de las acciones y respuestas.
const c = {
  updateDataOnload: function(){
    const idiomaSeleccionado = m.verDataLocalStorage()
    v.configButton(m[idiomaSeleccionado]);
    v.renderAlert(m[idiomaSeleccionado]);
    v.renderBody(m[idiomaSeleccionado]);
  }
};

window.onload = c.updateDataOnload;